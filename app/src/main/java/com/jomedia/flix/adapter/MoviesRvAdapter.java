package com.jomedia.flix.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.jomedia.flix.R;
import com.jomedia.flix.model.MovieModel;

import java.util.ArrayList;

public class MoviesRvAdapter extends RecyclerView.Adapter<MoviesRvAdapter.ViewHolder> {

    private final String category;
    private ArrayList<MovieModel> movies = new ArrayList<>();

    public MoviesRvAdapter(String category) {
        this.category = category;
        stubMethod();
    }

    private void stubMethod() {
        for (int i = 0; i < 10; i++) {
            MovieModel movie = new MovieModel();
            movie.setTitle(category + " " + i);
            movies.add(movie);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MovieModel m = movies.get(position);
//        holder.title.setText(m.getTitle());
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView iv;
//        public TextView title;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
//            title = (TextView) v.findViewById(R.id.movieTitle);
        }

        @Override
        public void onClick(View v) {
            Log.i("***", "" + getAdapterPosition());
        }
    }
}
