package com.jomedia.flix.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.jomedia.flix.R;
import com.jomedia.flix.model.MovieModel;

import java.util.ArrayList;

public class MoviesPagerAdapter extends PagerAdapter {

    private ArrayList<MovieModel> items = new ArrayList<>();

    public MoviesPagerAdapter() {
        stubMethod();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = View.inflate(container.getContext(), R.layout.movie_item_pager, null);

        TextView title = (TextView) view.findViewById(R.id.title);

        MovieModel item = items.get(position);
        title.setText(item.getTitle());
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return o == view;
    }

    private void stubMethod() {
        for (int i = 0; i < 10; i++) {
            MovieModel item = new MovieModel();
            item.setTitle("Movie " + i);
            items.add(item);
        }
    }
}

