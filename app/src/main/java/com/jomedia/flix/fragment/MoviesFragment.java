package com.jomedia.flix.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jomedia.flix.R;
import com.jomedia.flix.adapter.MoviesPagerAdapter;
import com.jomedia.flix.adapter.MoviesRvAdapter;
import com.jomedia.flix.util.Const;


public class MoviesFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        ViewPager topPager = (ViewPager) view.findViewById(R.id.topPager);
        topPager.setAdapter(new MoviesPagerAdapter());
        //
        RecyclerView moviesPopularRv = (RecyclerView) view.findViewById(R.id.moviesPopularRv);
        moviesPopularRv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        moviesPopularRv.setHasFixedSize(true);
        moviesPopularRv.setAdapter(new MoviesRvAdapter(getString(R.string.movie_category_popular)));
        moviesPopularRv.setNestedScrollingEnabled(false);
        //
        RecyclerView moviesLatestRv = (RecyclerView) view.findViewById(R.id.moviesLatestRv);
        moviesLatestRv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        moviesLatestRv.setHasFixedSize(true);
        moviesLatestRv.setAdapter(new MoviesRvAdapter(getString(R.string.movie_category_latest)));
        moviesLatestRv.setNestedScrollingEnabled(false);
        //
        TextView categoryPopularTv = (TextView) view.findViewById(R.id.moviesPopularTv);
        TextView categoryLatestTv = (TextView) view.findViewById(R.id.moviesLatestTv);
        categoryPopularTv.setOnClickListener(this);
        categoryLatestTv.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        Fragment frag = null;
        switch (v.getId()) {
            case R.id.moviesPopularTv:
                frag = MoviesCategoryFragment.newInstance(Const.MOVIES_CATEGORY_POPULAR);
                break;
            case R.id.moviesLatestTv:
                frag = MoviesCategoryFragment.newInstance(Const.MOVIES_CATEGORY_LATEST);
                break;
        }
        getFragmentManager().beginTransaction()
                .add(R.id.mainContainer, frag, Const.TAG_FRAGMENT_MOVIES_BY_CATEGORY)
                .addToBackStack(null).commit();
    }
}
