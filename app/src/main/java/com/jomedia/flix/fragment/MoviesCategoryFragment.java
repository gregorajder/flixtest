package com.jomedia.flix.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jomedia.flix.R;
import com.jomedia.flix.interfaces.IF_ShowTabs;
import com.jomedia.flix.util.Const;


public class MoviesCategoryFragment extends Fragment {

    private static final String CATEGORY = "CATEGORY";
    private IF_ShowTabs listener;

    public static MoviesCategoryFragment newInstance(String category) {
        Bundle args = new Bundle();
        args.putString(CATEGORY, category);
        MoviesCategoryFragment fragment = new MoviesCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies_by_category, container, false);
        TextView tv = (TextView) view.findViewById(R.id.moviesCategoryNameTv);
        initCategoryTextView(tv);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IF_ShowTabs) {
            listener = (IF_ShowTabs) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showTabs(false);
    }

    @Override
    public void onPause() {
        super.onResume();
        listener.showTabs(true);
    }

    //    private void initActionBar(Toolbar toolbar) {
//        setSupportActionBar(toolbar);
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayShowTitleEnabled(false);
//            actionBar.setLogo(R.drawable.logo);
//        }
//    }


    private void initCategoryTextView(TextView tv) {
        switch (getArguments().getString(CATEGORY, "")) {
            case Const.MOVIES_CATEGORY_POPULAR:
                tv.setText(getString(R.string.movie_category_popular));
                break;
            case Const.MOVIES_CATEGORY_LATEST:
                tv.setText(getString(R.string.movie_category_latest));
                break;
        }
    }

}
