package com.jomedia.flix;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

public class CustomTabLayout extends TabLayout implements TabLayout.OnTabSelectedListener {

    private final PorterDuffColorFilter selIconFilter;
    private final PorterDuffColorFilter deselIconFilter;

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        int selectedColor = ContextCompat.getColor(getContext(), R.color.tab_icon_selected);
        int deselectedColor = ContextCompat.getColor(getContext(), R.color.tab_icon_not_selected);
        selIconFilter = new PorterDuffColorFilter(selectedColor, PorterDuff.Mode.SRC_ATOP);
        deselIconFilter = new PorterDuffColorFilter(deselectedColor, PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void setupWithViewPager(ViewPager viewPager) {
        super.setupWithViewPager(viewPager);
        TabLayout.Tab tab0 = getTabAt(0);
        TabLayout.Tab tab1 = getTabAt(1);
        if (tab0 != null) tab0.setIcon(R.drawable.ic_videocam_24dp);
        if (tab1 != null) {
            tab1.setIcon(R.drawable.ic_import_contacts);
            Drawable icon = tab1.getIcon();
            if (icon != null) icon.setColorFilter(deselIconFilter);
        }
        setOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        Drawable icon = tab.getIcon();
        if (icon != null) icon.setColorFilter(selIconFilter);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        Drawable icon = tab.getIcon();
        if (icon != null) icon.setColorFilter(deselIconFilter);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }
}
