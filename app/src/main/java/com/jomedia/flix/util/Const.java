package com.jomedia.flix.util;

public final class Const {
    public static final String TAG_FRAGMENT_MOVIES_BY_CATEGORY = "TAG_FRAGMENT_MOVIES_BY_CATEGORY";
    public static final String MOVIES_CATEGORY_POPULAR = "MOVIES_CATEGORY_POPULAR";
    public static final String MOVIES_CATEGORY_LATEST = "MOVIES_CATEGORY_LATEST";

}
