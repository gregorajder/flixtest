package com.jomedia.flix;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.jomedia.flix.fragment.MoviesFragment;
import com.jomedia.flix.interfaces.IF_ShowTabs;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, IF_ShowTabs {

    private ViewPager tabPager;
    private DrawerLayout drawer;
    private CustomTabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        initActionBar(toolbar);
        initDrawerAndNavView(toolbar);
        initPager();
        initTabLayout();
    }

    private void initActionBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setLogo(R.drawable.logo);
        }
    }

    private void initPager() {
        tabPager = (ViewPager) findViewById(R.id.tabPager);
        if (tabPager != null) tabPager.setAdapter(new TabPagerAdapter(getSupportFragmentManager()));
    }

    private void initTabLayout() {
        tabLayout = (CustomTabLayout) findViewById(R.id.tabs);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(tabPager);
        }
    }

    private void initDrawerAndNavView(Toolbar toolbar) {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
        }
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void showTabs(boolean show) {
        if (show) tabLayout.setVisibility(View.VISIBLE);
        else tabLayout.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
//        SearchView searchItem = (SearchView) menu.findItem(R.id.action_search);
//        searchItem.setQueryHint("Search movies or books");
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.movies_drawer:
                tabPager.setCurrentItem(0);
                break;
            case R.id.books_drawer:
                tabPager.setCurrentItem(1);
                break;
            case R.id.bookmarks_drawer:

                break;

            case R.id.history_drawer:

                break;
            case R.id.settings_drawer:

                break;
        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class TabPagerAdapter extends FragmentPagerAdapter {

        private final int[] imageResId = {R.drawable.ic_videocam_24dp, R.drawable.ic_import_contacts};

        public TabPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new MoviesFragment();
                case 1:
                    return new MoviesFragment(); // TODO
            }
            return null;
        }

        @Override
        public int getCount() {
            return imageResId.length;
        }

    }
}
