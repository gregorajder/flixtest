package com.jomedia.flix.interfaces;

public interface IF_ShowTabs {
    void showTabs(boolean show);
}
